FROM continuumio/miniconda3
LABEL authors="Anthony Underwood, Varun Shamanna" \
      description="Docker image containing requirements for ariba as part of the GHRU AMR prediction pipeline"
RUN apt update
# install procps which is required by Nextflow trace
RUN apt install -y procps

## Install ariba via conda
COPY environment.yml /
RUN conda env create -f /environment.yml && conda clean -a
ENV PATH /opt/conda/envs/ghru-ariba/bin:$PATH
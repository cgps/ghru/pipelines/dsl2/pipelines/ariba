include './messages.nf'
include './params_utilities.nf'

def default_params(){
    /***************** Setup inputs and channels ************************/
    def params = [:]
    params.help = false
    params.version = false

    params.input_dir = false
    params.fastq_pattern = false
    params.output_dir = false

    params.database_dir = false
    params.summary_arguments = false

    return params
}

def check_params(Map params, String version) { 
    // set up input directory
    def final_params = [:]
    check_optional_parameters(params, ['input_dir', 'reads_path'])
    if (params.input_dir){
        def input_dir = check_mandatory_parameter(params, 'input_dir') - ~/\/$/

        //  check a pattern has been specified
        def fastq_pattern = check_mandatory_parameter(params, 'fastq_pattern')

        //
        final_params.reads_path = input_dir + "/" + fastq_pattern
    } else {
        final_params.reads_path = params.reads_path
    }


    // set up output directory
    final_params.output_dir = check_mandatory_parameter(params, 'output_dir') - ~/\/$/

    // ariba database - default is in container at /resfinder_database.17.10.2019
    if (params.database_dir){
        final_params.database_dir = file(params.database_dir)
    } else {
        final_params.database_dir = file("${workflow.projectDir}/ariba_databases/ncbi_db_2019-10-30.1")
    }

    // ariba summary columns (default ---cluster_cols assembled,match,ref_seq,pct_id,ctg_cov)
    if (params.summary_arguments){
        final_params.summary_arguments = params.summary_arguments
    } else {
        final_params.summary_arguments = '--cluster_cols assembled,match,ref_seq,pct_id,ctg_cov'
    }
    return final_params
}


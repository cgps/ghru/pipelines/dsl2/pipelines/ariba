def version_message(String version) {
    println(
        """
        ==============================================
        Resfinder Pipeline version ${version}
        ==============================================
        """.stripIndent()
    )
}

def help_message() {
    println(
        """
        Mandatory arguments:
        --input_dir                 Path to input dir. This must be used in conjunction with fastq_pattern
        --fastq_pattern             The regular expression that will match fastq files e.g '*_{1,2}.fastq.gz'
        --output_dir                Path to output dir
 
        Optional arguments:
        --database_dir   Path to a local dir containing ariba resitance database (default is /resfinder_database.17.10.2019)
        --summary_arguments Supply the non-default options for the ariba summary command.
            You will need to specify these with --summary_arguments and wrap thme in quotes e.g '--preset minimal --min_id 95'
            (default is '--cluster_cols assembled,match,ref_seq,pct_id,ctg_cov')
        """.stripIndent()
    )
}

def complete_message(Map params, nextflow.script.WorkflowMetadata workflow, String version){
    // Display complete message
    println ""
    println "Ran the workflow: ${workflow.scriptName} ${version}"
    println "Command line    : ${workflow.commandLine}"
    println "Completed at    : ${workflow.complete}"
    println "Duration        : ${workflow.duration}"
    println "Success         : ${workflow.success}"
    println "Work directory  : ${workflow.workDir}"
    println "Exit status     : ${workflow.exitStatus}"
    println ""
    println "Parameters"
    println "=========="
    params.each{ k, v ->
        if (v){
            println "${k}: ${v}"
        }
    }
}

def error_message(nextflow.script.WorkflowMetadata workflow){
    // Display error message
    println ""
    println "Workflow execution stopped with the following message:"
    println "  " + workflow.errorMessage

}
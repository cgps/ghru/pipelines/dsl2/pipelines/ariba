// DSL 2
nextflow.preview.dsl=2

include './lib/messages'
include './lib/params_parser'
include './lib/params_utilities'

// version
version = '1.0'
// setup params
default_params = default_params()
merged_params = default_params + params

// help and version messages
help_or_version(merged_params, version)
final_params = check_params(merged_params, version)

include './lib/modules/workflows' params(final_params)
workflow {
//Setup input Channel from Read path
  Channel
      .fromFilePairs( final_params.reads_path )
      .ifEmpty { error "Cannot find any reads matching: ${final_params.reads_path}" }
      .set { reads }
    
   ariba(reads, final_params.database_dir, final_params.summary_arguments)
}